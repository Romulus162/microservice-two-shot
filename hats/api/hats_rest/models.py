from django.db import models


# Create your models here.
class LocationVO(models.Model):
    href = models.CharField(max_length=50, unique=True)
    closet_name = models.CharField(max_length=50)
    section_number = models.PositiveSmallIntegerField(null=True)
    shelf_number = models.PositiveSmallIntegerField(null=True)

    def __str__(self):
        return self.closet_name

class Hat(models.Model):
    fabric = models.CharField(max_length=50)
    style = models.CharField(max_length=50)
    color = models.CharField(max_length=50)
    pictureUrl = models.URLField(null=True)
    location = models.ForeignKey(
        LocationVO, related_name="hat", on_delete=models.CASCADE,
    )
