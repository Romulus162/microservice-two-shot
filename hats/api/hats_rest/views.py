from django.shortcuts import render
from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Hat, LocationVO
from django.views.decorators.http import require_http_methods
import json
# Create your views here.
class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "closet_name", "href", "section_number", "shelf_number"
    ]

class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric", "style", "color", "pictureUrl", "location",
    ]
    encoders = {
        "location": LocationVOEncoder()
    }

class HatListEncoder(ModelEncoder):
    model = Hat
    properties  = [
        "style", "pictureUrl", "id", "fabric", "color"
    ]
    def get_extra_data(self, o):
        return {"location": o.location.id}

@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse({"hats": hats}, HatListEncoder)
    else:
        content = json.loads(request.body)
        try:
            location_id = content["location"]
            location = LocationVO.objects.get(href=location_id)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid"},
                status=400
            )
        hat = Hat.objects.create(**content)
        return JsonResponse(hat, HatDetailEncoder, safe=False)

@require_http_methods(["GET", "PUT","DELETE"])
def api_hat_details(request, pk):
    if request.method == "GET":
        hat = Hat.objects.get(id=pk)
        return JsonResponse(hat, encoder=HatDetailEncoder, safe=False)
    elif request.method == "PUT":
        content = json.loads(request.body)
        try:
            if "location" in content:
                location_href = content["location"]
                location = LocationVO.objects.get(href=location_href)
                content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid"},
                status=400
            )
        Hat.objects.filter(id=pk).update(**content)
        hat = Hat.objects.get(id=pk)
        return JsonResponse(hat, HatDetailEncoder, False)
    else:
        count, _ = Hat.objects.filter(id=pk).delete()
        return JsonResponse({"delete": count > 0})
