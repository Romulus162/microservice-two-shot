import React, { useEffect, useState } from 'react';
import Hatnav from './Hatnav.js'

function HatForm({locations}) {
    const [style, setStyle] = useState('');
    const [fabric, setFabric] = useState('');
    const [color, setColor] = useState('');
    const [PictureUrl, setPictureUrl] = useState('');
    const [location, setLocation] = useState('');

    const handleStyleChange = (event) => {
        const value = event.target.value;
        setStyle(value);
    }

    const handleFabricChange = (event) => {
        const value = event.target.value;
        setFabric(value);
    }

    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const handlePictureUrlChange = (event) => {
        const value = event.target.value;
        setPictureUrl(value);
    }

    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/locations/'

        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations)
        }
    }

    const handleSubmit = async (e) => {
        e.preventDefault();

        const data = {};

        data.style_name = styleName;
        data.fabric = fabric;
        data.color = color;
        data.Picture = PictureUrl;
        data.location = location;

        console.log(data);
        const locationUrl = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            const newHat = await response.json();
            console.log(newHat);
            setStyleName('');
            setFabric('');
            setColor('');
            setPictureURL('');
            setLocation('');
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
            <div className='offset-3 col-6'>
                <div className="shadow p-4 mt-4">
                    <h1>Add a New Hat</h1>
                    <form onSubmit={handleSubmit} id="create-hat-form">
                        <div className="form-floating mb-3">
                            <input value={style} onChange={handleStyleChange} placeholder="Style" required type="text" name="style" id="style" className="form-control"/>
                            <label htmlFor="style_name">Style Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={fabric} onChange={handleFabricChange} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control"/>
                            <label htmlFor='fabric'>Fabric</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={color} onChange={handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                            <label htmlFor='color'>Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={PictureUrlL} onChange={handlePictureUrlChange} placeholder="Picture" required type="text" name="picture" id="picture" className="form-control"/>
                            <label htmlFor='picture'>PictureUrl</label>
                        </div>
                        <div className="mb-3">
                            <select value={location} onChange={handleLocationChange} required id="location" name="location" className="form-select">
                                <option value="">Closet Location</option>
                                {locations.map(location => {
                                    return (
                                        <option key={location.id} value={location.id}>
                                            {location.closet_name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default HatForm;
